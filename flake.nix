{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs =
    { self, nixpkgs }:
    rec {
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      kernel = pkgs.linux_latest;
      packages.x86_64-linux.default = pkgs.stdenv.mkDerivation {
        pname = "msi-ec";
        version = "unstable-2023-10-06";

        src = pkgs.fetchFromGitHub {
          owner = "BeardOverflow";
          repo = "msi-ec";
          rev = "c39fe616c24e2ff12f086dfbc34358499c13f6bf";
          hash = "sha256-m7xrv+FvggYHIvtysdY9M2BWL8WkE57aM7RWEmHp2m4=";
        };

        NIX_CFLAGS_COMPILE = "-Wno-error=incompatible-pointer-types";

        nativeBuildInputs = kernel.moduleBuildDependencies;
        enableParallelBuilding = true;

        postPatch = ''
          substituteInPlace Makefile \
            --replace '/lib/modules/$(shell uname -r)/extra' "$out/lib/modules/${kernel.modDirVersion}/extra" \
            --replace '/lib/modules/$(shell uname -r)/build' "${kernel.dev}/lib/modules/${kernel.modDirVersion}/build" \
            --replace 'depmod -a' "" \
            --replace 'echo msi-ec > /etc/modules-load.d/msi-ec.conf' "" \
            --replace 'modprobe -v msi-ec' ""
        '';
      };
    };
}
